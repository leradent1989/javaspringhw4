package com.example.bootdata;


import com.example.bootdata.dao.CustomerJpaRepository;
import com.example.bootdata.repository.UserRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.CustomerDto;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.service.CustomOAuth2UserService;
import com.example.bootdata.service.WebSecurityDbFormConfig;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
//mport org.springframework.security.crypto.password.PasswordEncoder;
import java.util.Set;

import static org.modelmapper.config.Configuration.AccessLevel.PRIVATE;


//@EnableJpaAuditing
@EnableTransactionManagement
@EnableWebSecurity
@SpringBootApplication
@Import(WebSecurityDbFormConfig.class)
public class BootDataApplication implements ApplicationRunner {
    @Autowired
    private CustomOAuth2UserService oauthUserService;
  /*  @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldMatchingEnabled(true)
                .setSkipNullEnabled(true)
                .setFieldAccessLevel(PRIVATE);
        mapper.createTypeMap(Customer.class, CustomerDto.class)
                .addMapping(Customer::getId, CustomerDto::setId)
                .addMapping(Customer::getName, CustomerDto::setName);
        return mapper;
    }*/
    public static void main(String[] args) {
        SpringApplication.run(BootDataApplication.class, args);
    }




    @Override
   // @Transactional
    public void run(ApplicationArguments args) {
        System.out.println("http://localhost:9000/swagger-ui/index.html \n");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
     //  return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }



    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("EIS API")
                        .description("Employee Information System sample application")
                        .version("v0.0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                        .description("SpringShop Wiki Documentation")
                        .contact(new Contact().email("test@test.com").url("http://fullstackcode.dev")))
                ;
    }
}
