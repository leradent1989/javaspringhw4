package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.*;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;

import com.example.bootdata.domain.hr.Employer;
import com.example.bootdata.service.AccountService;
import com.example.bootdata.service.CustomerService;

import com.example.bootdata.service.dtomapper.*;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
@CrossOrigin( origins = {"http://localhost:3000"})
public class CustomerRestController {

    private final CustomerService customerService;
    private final AccountService accountService ;

    private final CustomerDtoMapper dtoMapper;

    private final CustomerRequestDtoMapper requestMapper;

    private  final AccountDtoMapper accountMapper;

    private  final EmployerDtoMapper employerMapper;

    private final AccountRequestDtoMapper requestAccountMapper;
    @GetMapping

    public List<CustomerDto> findAll(){
        return customerService.findAll(0,10).stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<Customer> departments = customerService.findAll(page, size);
        List<CustomerDto> departmentsDto = departments.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(departmentsDto);
    }
    @GetMapping("/{id}/accounts")
    public ResponseEntity<?> getCustomerAccounts(@PathVariable ("id") Long customerId){
        Customer customer = customerService.getOne(customerId);
        List <Account > customerAccounts = customer.getAccounts();

        if (customer == null){
            return ResponseEntity.badRequest().body("Customer not found");
        }
        List < AccountDto> customerAccountsDto = customerAccounts.stream().map(accountMapper ::convertToDto).collect(Collectors.toList());
        return ResponseEntity.ok().body(customerAccountsDto );
    }
    @GetMapping("/{id}/employers")
    public ResponseEntity<?> getCustomerCompanies(@PathVariable ("id") Long customerId){
        Customer customer = customerService.getOne(customerId);
        List <Employer > customerCompanies = customer.getEmployers();

        if (customer == null){
            return ResponseEntity.badRequest().body("Customer not found");
        }
        List <EmployerDto> customerEmployerDto = customerCompanies.stream().map(employerMapper ::convertToDto).collect(Collectors.toList());
        return ResponseEntity.ok().body(customerEmployerDto );
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long customerId){
        Customer customer = customerService.getOne(customerId);
        if (customer  == null){
            return ResponseEntity.badRequest().body("Student not found");
        }
        System.out.println(customer.getEmployers());
        return ResponseEntity.ok().body(dtoMapper.convertToDto(customer) );
    }

    @PostMapping
    public void create( @Valid  @RequestBody CustomerRequestDto customer ){
        customerService.save(requestMapper.convertToEntity(customer) );
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody CustomerRequestDto customerRequestDto ){
        try {
         //  Customer customer =requestMapper.convertToEntity(customerRequestDto);
          //  customer.setCreationDate(customerService.getOne(customer.getId()).getCreationDate());
  customerService.update(requestMapper.convertToEntity(customerRequestDto));

      return ResponseEntity.ok().build();


        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable ("id") Long customerId){
        try {
            customerService.deleteById(customerId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
      @DeleteMapping

    public ResponseEntity <?> deleteCustomer(@RequestBody CustomerRequestDto customer){

        try{

            customerService.delete(requestMapper.convertToEntity(customer));
         return    ResponseEntity.ok().build();

        }catch (RuntimeException e){

            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }



    @PutMapping("/account/{id}")

    public ResponseEntity <?> addAccount(@PathVariable Long id,@RequestBody AccountRequestDto accountRequestDto ){


        Account account = requestAccountMapper.convertToEntity(accountRequestDto);

        if(customerService.addAccount(id,account) == false){
            return ResponseEntity.badRequest().body("Incorrect account number");
        }else{
            return ResponseEntity.ok().build();
        }



    }
    @DeleteMapping("/account/{id}")

    public ResponseEntity <?> deleteAccount(@PathVariable  Long id, @RequestBody AccountRequestDto accountRequestDto ){
        Account account = requestAccountMapper.convertToEntity(accountRequestDto);

       if(accountService.getOne(id) == null){
            return ResponseEntity.badRequest().body("Account does not exist");
        }
        Customer customer = customerService.getOne(id);

        List <Account > customerAccounts = customer.getAccounts();


        if(!customerAccounts.contains(accountService.getOne(account.getId()))){
            return ResponseEntity.badRequest().body("This account isn't in the list ");
        }
        Account account1 =accountService.getOne(account.getId());
        account1.setCustomer(null);
        customerAccounts.remove(accountService.getOne(account.getId()));
        customer.setAccounts(customerAccounts);

        customerService.update(customer);
        accountService.update(account1);
        return ResponseEntity.ok().build()  ;



    }





}