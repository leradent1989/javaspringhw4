package com.example.bootdata.service;


import com.example.bootdata.domain.hr.Employer;

import java.util.List;

public interface EmployerService {
    List<Employer> findAll(Integer page, Integer size);
    List<Employer> getAll();
     void create(Employer employee);
    Employer getById(Long userId);
    void deleteById(Long id );
    void update(Employer employee);
    void delete(Employer employee );
}
