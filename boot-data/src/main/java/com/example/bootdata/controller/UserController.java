package com.example.bootdata.controller;

import com.example.bootdata.domain.dto.UserDto;
import com.example.bootdata.service.UserDetailsServiceImpl;
import com.example.bootdata.service.UserDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserDetailsServiceImpl userDetailsService;
    private final UserDtoMapper userDtoMapper;
    // For basic auth
    @GetMapping("/dashboard")
    public String basicAuth(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        model.addAttribute("employees", new String[]{"ddff"});
        printSecurityUserName();

    return "dashboard";
    }

    private String printSecurityUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        System.out.println(name);


        return name;
    }


    @GetMapping("/login")
    public String userDetails(Model model) {
        model.addAttribute("employees", new String[]{"ddff"});

        return "/login";
    }



    @GetMapping("/registration")
    public String reg(){
        return "registration";
    }

    @PostMapping("/registration")

    public String registrate(@RequestParam String username, @RequestParam String password ) {
        userDetailsService.createNew(username, password);


        return "/login";
    }
    @GetMapping("/users")
    @ResponseBody
    public List<UserDto> getAll() {
        return userDetailsService.findAll().stream()
                .map(userDtoMapper::convertToDto)
                .toList();
    }


}
