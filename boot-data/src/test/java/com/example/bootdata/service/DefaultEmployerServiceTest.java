package com.example.bootdata.service;

import com.example.bootdata.dao.CustomerJpaRepository;

import com.example.bootdata.dao.EmployerJpaRepository;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.domain.hr.Employer;
import com.example.bootdata.service.DefaultCustomerService;
import com.example.bootdata.service.DefaultEmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultEmployerServiceTest {

    @Mock
    private EmployerJpaRepository employerJpaRepository;


    @InjectMocks
    private DefaultEmployeeService employerService;

    @Captor
    private ArgumentCaptor<Employer> employerArgumentCaptor;

    @Test
    public void testGetAllPageble() {
        Employer employer = new Employer();
        when(employerJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(employer)));
        List<Employer> employers = employerService.findAll(1, 2);

        assertEquals(employer, employers.get(0));
    }

    @Test
    public void test_GetAll_Success() {
       Employer employer1 = new Employer();
       Employer employer2 = new Employer();
        List<Employer> employersExpected = List.of(employer1, employer2);
        when(employerJpaRepository.findAll())
                .thenReturn(employersExpected);

        List<Employer> employersActual =employerService.getAll();
        assertNotNull(employersActual);
        assertFalse(employersActual.isEmpty());
        assertIterableEquals(employersExpected, employersActual);
    }
    @Test
    public void test_GetById_Success() {
        Employer employer1 = new Employer();
        employer1.setId(1L);
        Employer employer2 = new Employer();
        employer2.setId(2L);
        Employer employerExpected = employer1;
        when(employerJpaRepository.getById(employer2.getId()))
                .thenReturn(employer2);

        Employer employerActual =employerService.getById(employer2.getId());
        assertNotNull(employerActual);

        assertEquals(employerExpected, employerActual);
    }

    @Test
    public void test_Create_Success() {
       Employer employer1 = new Employer();

       employerService.create(employer1);

        verify(employerJpaRepository).save(employerArgumentCaptor.capture());
        Employer employerActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employer1, employerActualArgument);
    }
    @Test
    public void test_Put_Success() {
        Employer employer1= new Employer() ;


        employer1.setName("Google");
        employer1.setCreationDate(new Date());
        when(employerJpaRepository.getById(employer1.getId())
        )
                .thenReturn(employer1);
        employerService.update(employer1);

        verify(employerJpaRepository).save(employerArgumentCaptor.capture());
       Employer   employerActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employer1, employerActualArgument);
    }
    @Test
    public void test_Delete_Success() {
        Employer employer1= new Employer() ;

        employerService.create(employer1);
       employer1.setId(1L);

        employer1.setName("Google");
        employerService.delete(employer1);

        verify(employerJpaRepository).delete(employerArgumentCaptor.capture());
        Employer  employerActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employer1, employerActualArgument);
    }


}